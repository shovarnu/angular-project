import { Component } from '@angular/core';
import {SuperHero} from './superheroes';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  
  
  

  weapons = ['Iron Suit', 'Hammer', 
  'Bow And Arrow', 'Shield', 'Raw Power'];

  specialities = ['Technology', 'God of Thunder',
  'Archery', 'War & Leadership', 'Scientist'];

  model = new SuperHero(1, "Iron Man",this.weapons[0],this.specialities[0],"Tony Stark");

  submitted = false;
  onSubmit()
  {
    this.submitted=true;
    console.log('this.model.name' + this.model.name);
    console.log('this.model.weapon' + this.model.weapon);
    console.log('this.model.speciality' + this.model.speciality);
  }

  


}
